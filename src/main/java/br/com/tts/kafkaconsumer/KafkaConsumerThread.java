package br.com.tts.kafkaconsumer;


import java.io.FileInputStream;
import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class KafkaConsumerThread extends Thread {
	
	private static final Logger logger = LoggerFactory.getLogger(KafkaConsumerThread.class);
	
	private String name;
	private Properties properties;
	private String[] topic;
	
	
	public KafkaConsumerThread(String name, String topic, Properties properties) {
		this.name = name;
		
		this.topic = new String[1];
		
		this.topic[0] = topic;
		this.properties = properties;
	}
	
	public KafkaConsumerThread(String name, String[] topic, Properties properties) {
		this.name = name;
		this.topic = topic;
		this.properties = properties;
	}
	
	@Override
	public void run() {
		KafkaConsumer<String, String> consumer = null;
		int endExecution = 0;
				
		logger.info(name + " begin");
		
		Properties execProperties = new Properties();
		try {
			execProperties.load(new FileInputStream("properties"));
			endExecution = Integer.valueOf(execProperties.getProperty("producer.endExecution"));
			consumer = new KafkaConsumer<String, String>(properties);    	
	    	
			//print the topic name
	        for (String string : topic) {
	        	logger.info(name + " subscribed to topic " + string);	        
			}
			
			consumer.subscribe(Arrays.asList(topic));
		} catch (Exception e) {
			endExecution = 1;
			logger.info(name + " error");
			e.printStackTrace();
		}
				    			
		while(endExecution == 0) {
								    
		    		    		
			logger.info(name + " running");
									
			//Kafka Consumer subscribes list of topics here.
	        	        		               	        
        	        	        	
        	logger.info(name + " receiving");
        	
            ConsumerRecords<String, String> records = consumer.poll(Duration.ofSeconds(30));
            for (ConsumerRecord<String, String> record : records)
             	logger.info(name + " " + "offset = " +  record.offset() + " key = " + record.key() + " value = " + record.value());
                        	        	        
	        try {
				sleep(5000);
			} catch (InterruptedException e) {
				endExecution = 1;
				logger.info(name + " error");
				e.printStackTrace();
			}
		}    
        
		consumer.close();
		
		logger.info(name + " end");
	
	}

}