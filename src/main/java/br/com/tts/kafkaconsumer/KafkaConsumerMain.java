package br.com.tts.kafkaconsumer;

import java.util.Properties;

public class KafkaConsumerMain {
    
	public static void main(String[] args) {
		       
		System.out.println("Main Begin");
		
		Properties properties = new Properties();

        properties.put("bootstrap.servers", "localhost:9092");
        properties.put("acks", "all");
        properties.put("retries", 0);
        properties.put("batch.size", 16384);
        properties.put("linger.ms", 1);
        properties.put("buffer.memory", 33554432);
        properties.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.put("group.id", "group_bat");    
        
        String[] topics = new String[2];
        
        topics[0] = "TOPIC1";
        //topics[1] = "TOPIC2";
        KafkaConsumerThread kct1 = new KafkaConsumerThread("CP1",topics[0],properties);
        
        //topics[0] = "TOPIC2";
        //topics[1] = "TOPIC3";
        //KafkaConsumerThread kct2 = new KafkaConsumerThread("CP2",topics,properties);                
        
        kct1.start();
        //kct2.start();
                                
        System.out.println("Main  End");
    }
}
